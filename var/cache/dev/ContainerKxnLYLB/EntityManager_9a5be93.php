<?php

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder2ebad = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer88598 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesc7bea = [
        
    ];

    public function getConnection()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'getConnection', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'getMetadataFactory', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'getExpressionBuilder', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'beginTransaction', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'getCache', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->getCache();
    }

    public function transactional($func)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'transactional', array('func' => $func), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->transactional($func);
    }

    public function commit()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'commit', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->commit();
    }

    public function rollback()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'rollback', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'getClassMetadata', array('className' => $className), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'createQuery', array('dql' => $dql), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'createNamedQuery', array('name' => $name), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'createQueryBuilder', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'flush', array('entity' => $entity), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'clear', array('entityName' => $entityName), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->clear($entityName);
    }

    public function close()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'close', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->close();
    }

    public function persist($entity)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'persist', array('entity' => $entity), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'remove', array('entity' => $entity), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'refresh', array('entity' => $entity), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'detach', array('entity' => $entity), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'merge', array('entity' => $entity), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'getRepository', array('entityName' => $entityName), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'contains', array('entity' => $entity), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'getEventManager', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'getConfiguration', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'isOpen', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'getUnitOfWork', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'getProxyFactory', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'initializeObject', array('obj' => $obj), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'getFilters', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'isFiltersStateClean', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'hasFilters', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return $this->valueHolder2ebad->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer88598 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder2ebad) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder2ebad = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder2ebad->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, '__get', ['name' => $name], $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        if (isset(self::$publicPropertiesc7bea[$name])) {
            return $this->valueHolder2ebad->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2ebad;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder2ebad;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2ebad;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder2ebad;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, '__isset', array('name' => $name), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2ebad;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder2ebad;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, '__unset', array('name' => $name), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2ebad;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder2ebad;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, '__clone', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        $this->valueHolder2ebad = clone $this->valueHolder2ebad;
    }

    public function __sleep()
    {
        $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, '__sleep', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;

        return array('valueHolder2ebad');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer88598 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer88598;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer88598 && ($this->initializer88598->__invoke($valueHolder2ebad, $this, 'initializeProxy', array(), $this->initializer88598) || 1) && $this->valueHolder2ebad = $valueHolder2ebad;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder2ebad;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHolder2ebad;
    }
}
