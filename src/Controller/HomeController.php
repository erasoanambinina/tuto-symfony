<?php
/**
 * Created by PhpStorm.
 * User: tinamalala
 * Date: 6/8/21
 * Time: 8:58 PM
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class HomeController extends AbstractController
{

    /**
     * @Route("/", name="home")
     * @return Response
     */
    public function  index(): Response
    {
        return $this->render('pages/home.html.twig');
    }
}