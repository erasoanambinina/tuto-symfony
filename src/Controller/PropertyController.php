<?php
/**
 * Created by PhpStorm.
 * User: tinamalala
 * Date: 6/10/21
 * Time: 7:30 AM
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PropertyController extends  AbstractController
{
    /**
     * @Route("/biens", name="property_index")
     * @return Response
     */
    public function index():Response
    {
        return $this->render('property/index.html.twig',[
            'current_menu' => 'properties'
        ]);
    }
}